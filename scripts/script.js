$(function () {

  var postsUrl = 'http://localhost:3000/posts';
  var postList = $('#postList');
  var posts = [];
  var saveButton = $('#save');
  var editButton = $('#edit');
  function insertPosts(posts) {
    // console.log(posts);
    $.each(posts, function (i, post) {
      var id = post.id;
      var container = $("<div class='myRow borderBottom' data-id='" + post.id + "'></div>")

      var title = $("<div class='col-9 postTitle'></div>").text(post.title);
      var buttons = $('<div class="col-3 buttons"><button type="button" class="btn btn-primary btn-xs detail" data-toggle="modal" data-target="#myModal">Wyświetl</button><button type="button" class="btn btn-primary btn-xs remove">Usuń</button></div>')

      container.append(title).append(buttons);
      postList.append(container);
    });
  };

  function loadPost() {
    $.ajax({
      url: postsUrl
    }).done(function (response) {
      insertPosts(response);
      posts = response;
    }).fail(function (error) {
      console.log(error);
    })
  }
  loadPost();
  var buttonAddNew = $('.inputButton');
  buttonAddNew.on('click', addNewPost);
  function addNewPost() {
    var name = $('#name').val();
    var age = $('#age').val();
    var sex = $('#sex').val();
    var title = $('#titlePost').val();
    var bodyPost = $('#bodyPost').val();
    var data = {
      title: title,
      user: name,
      age: age,
      sex: sex,
      body: bodyPost
    };
    $.ajax({
      type: 'POST',
      url: postsUrl,
      data
    }).done(function (response) {
      insertPosts([response]);
      $('input').val(' ');
      $('textarea').val(' ');
      posts.push(response);
      // getPosts();
    }).fail(function (error) {
      console.log(error);
    });
  };
  postList.on('click', '.remove', function () {
    var postId = $(this).parent().parent().data('id');
    var postUrl = postsUrl + '/' + postId;
    $.ajax({
      type: 'DELETE',
      url: postUrl
    }).done(function () {
      $(this).parent().parent().remove();
    }.bind(this)).fail(function (error) {
      console.log(error);
    });
  });

  var modalUser = $('.user');
  var modalTitle = $('.modal-title');
  var modalBodyPost = $('.bodyPost');
  postList.on('click', '.detail', function () {
    saveButton.hide();
    editButton.show();
    var postId = $(this).parent().parent().data('id');
    // console.log(posts);
    var post = posts.find(function (el) {
      if (el.id === postId) {
        return true
      };
    });
    var modalId = $('#modalId').text(postId).hide();
    modalUser.text('Użytkownik: ' + post.user + ', ' + post.age + ' lat, ' + post.sex);
    modalTitle.text(post.title);
    modalBodyPost.text(post.body);
  });

  editButton.on('click', function () {
    saveButton.show();
    editButton.hide();
    var postId = $('#modalId').text();
    var post = posts.find(function (el) {
      if (el.id == postId) {
        return true
      };
    });
    // console.log(post);
    var editUser = $('<label>Użytkownik:</label> <input id="newUser" class="input-s" value="' + post.user + '"></input><label>Wiek:</label> <input id="newAge" class="input-s" value="' + post.age + '"></input><label>Płeć:</label> <input id="newSex" class="input-s" value="' + post.sex + '"></input>');
    var editTitle = $('<label>Tytuł:</label> <input class="input-s" value="' + post.title + '"></input>');
    var editBodyPost = $('<label>Twój post:</label> <textarea class="textareaNewPost">' + post.body + '</textarea>')
    modalUser.text('').append(editUser);
    modalTitle.text('').append(editTitle);
    modalBodyPost.text('').append(editBodyPost);
  });
  saveButton.on('click', function () {
    saveButton.hide();
    editButton.show();
    // console.log($(this).parent().parent().find('.user'));
    var newUser = $('#newUser').val();
    var newAge = $('#newAge').val();
    var newSex = $('#newSex').val();
    var editedUser = modalUser.text('Użytkownik: ' + newUser + ', ' + newAge + ' lat, ' + newSex);
    var newTitle = modalTitle.find('input').val();
    modalTitle.text(newTitle);
    var newBodyPost = modalBodyPost.find('textarea').val();
    modalBodyPost.text(newBodyPost);
    var postId = $('#modalId').text();
    var post = posts.find(function (el) {
      if (el.id == postId) {
        return true
      };
    });
    var postDetail = {
      title: newTitle,
      user: newUser,
      age: newAge,
      sex: newSex,
      body: newBodyPost
    }
    changePost(postDetail, postId);
  });

  function changePost(data, id) {
    var postUrl = postsUrl + '/' + id;
    $.ajax({
      type: 'PUT',
      url: postUrl,
      data
    }).done(function () {
      var post = posts.find(function (el) {
        if (el.id == id) {
          return true
        };
      });
      Object.assign(post, data);
      var editTitleOnList = $('.borderBottom[data-id="' + id + '"]').find('.postTitle').text(post.title);
    }).fail(function (error) {
      console.log(error);
    });
  }

  // function getPosts() {
  //   $.ajax({
  //     url: postsUrl
  //   }).done(function (response) {
  //     // insertPosts(response);
  //     posts = response;
  //   }).fail(function (error) {
  //     console.log(error);
  //   });
  // }
});